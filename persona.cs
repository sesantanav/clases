﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Objetos
{
    class persona
    {
        private string nombre;
        private string apellido;
        private string run;
        private string fechaNacimiento;

        // constructores
        public persona() { }

        public persona(string _nombre)
        {
            this.nombre = _nombre;
        }

        public persona(string _nombre, string _apellido, string _run, string _fecha)
        {
            this.nombre = _nombre;
            this.apellido = _apellido;
            this.run = _run;
            this.fechaNacimiento = _fecha;
        }
        // setters
        public void setNombre(string _nombre)
        {
            this.nombre = _nombre;
        }
        public void setApellido(string _apellido)
        {
            this.apellido = _apellido;
        }

        // getters
        public string getNombre()
        {
            return this.nombre;
        }

        public string getNombreCompleto()
        {
            return this.nombre + this.apellido;
        }

        public void getimprimirNombreC()
        {
            Console.WriteLine("El nombre completo es: " + this.nombre + " " + this.apellido);
        }
        public void getInformacion()
        {
            Console.WriteLine("Información de la persona: ");
            Console.WriteLine("El nombre es: " + this.nombre);
        }



    }

}
