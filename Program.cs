﻿using System;

using static Objetos.alumnos;

namespace Objetos
{
    class Program
    {
        static void Main(string[] args)
        {
            alumnos alumno1 = new alumnos();
            alumnos alumno2 = new alumnos();

            alumno1.setNombre("Marcos");
            alumno1.setCarrera("Ingeniería Informática");
            string nombre = alumno1.getNombre();
            alumno1.getCarrera();
            Console.WriteLine(nombre);
            alumno1.getInformacion();

            alumno2.setNombre("Nicolas");
            alumno2.getInformacion();

            Console.WriteLine("***********************");
            alumno1.setCarrera("Tecnico en Programación"); 
            alumno1.getCarrera();
        }
    }
}
